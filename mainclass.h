#pragma once

#include <QtWidgets/QMainWindow>
#include <QCheckBox>
#include "ui_mainclass.h"
#include "settings.h"

class MainClass : public QMainWindow
{
	Q_OBJECT

public:
	MainClass(QWidget *parent = Q_NULLPTR);
	bool lkserversmirror;
	bool developer;
public slots:
	void openSettings();
	void setTerramenuCheck();
	void setForgeCheck();
	int sumnrun();

private:
	Ui::MainClassClass ui;
	bool optifine;
	bool forge;
	bool menu;
	int i;
};
