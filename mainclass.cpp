#include "mainclass.h"
#include <cstdlib>
#include <settings.h>

MainClass::MainClass(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	connect(ui.quit_button, SIGNAL(clicked()), this, SLOT(close()));
	connect(ui.settings_button, SIGNAL(clicked()), this, SLOT(openSettings()));
	connect(ui.forge_check, SIGNAL(stateChanged(int)), this, SLOT(setTerramenuCheck()));
	connect(ui.terramenu_check, SIGNAL(stateChanged(int)), this, SLOT(setForgeCheck()));
	connect(ui.continue_button, SIGNAL(clicked()), this, SLOT(sumnrun()));
}

void MainClass::openSettings() {
	Settings s;
	s.setModal(true);
	s.exec();
}

void MainClass::setTerramenuCheck() {
	if (ui.terramenu_check->isChecked() && ui.forge_check->isChecked() == false) {
		ui.terramenu_check->setChecked(false);
	}
}

void MainClass::setForgeCheck() {
	if (ui.terramenu_check->isChecked() && ui.forge_check->isChecked() == false) {
		ui.forge_check->setChecked(true);
	}
}

int MainClass::sumnrun() {
	Settings s;
	lkserversmirror = s.getLkm();
	developer = s.getDev();
	return 0;
}