/********************************************************************************
** Form generated from reading UI file 'settings.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGS_H
#define UI_SETTINGS_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_Settings
{
public:
    QVBoxLayout *verticalLayout_4;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *lkmirror_check;
    QCheckBox *dev_check;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QLabel *label;

    void setupUi(QDialog *Settings)
    {
        if (Settings->objectName().isEmpty())
            Settings->setObjectName(QString::fromUtf8("Settings"));
        Settings->resize(448, 257);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(Settings->sizePolicy().hasHeightForWidth());
        Settings->setSizePolicy(sizePolicy);
        Settings->setMaximumSize(QSize(448, 257));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/Resources/icons8-t-96.png"), QSize(), QIcon::Normal, QIcon::Off);
        Settings->setWindowIcon(icon);
        verticalLayout_4 = new QVBoxLayout(Settings);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        lkmirror_check = new QCheckBox(Settings);
        lkmirror_check->setObjectName(QString::fromUtf8("lkmirror_check"));

        verticalLayout_2->addWidget(lkmirror_check);

        dev_check = new QCheckBox(Settings);
        dev_check->setObjectName(QString::fromUtf8("dev_check"));

        verticalLayout_2->addWidget(dev_check);


        verticalLayout_3->addLayout(verticalLayout_2);

        groupBox = new QGroupBox(Settings);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);


        verticalLayout_3->addWidget(groupBox);


        verticalLayout_4->addLayout(verticalLayout_3);


        retranslateUi(Settings);

        QMetaObject::connectSlotsByName(Settings);
    } // setupUi

    void retranslateUi(QDialog *Settings)
    {
        Settings->setWindowTitle(QApplication::translate("Settings", "Einstellungen", nullptr));
        lkmirror_check->setText(QApplication::translate("Settings", "Benutze die LK-Servers Server", nullptr));
        dev_check->setText(QApplication::translate("Settings", "Developer Modus", nullptr));
        groupBox->setTitle(QApplication::translate("Settings", "Was bewirkt das?", nullptr));
        label->setText(QApplication::translate("Settings", "<html><head/><body><p><span style=\" font-size:10pt; font-weight:600;\">LK-Servers Server:</span><span style=\" font-size:10pt;\"> Anstelle der offiziellen Server </span></p><p><span style=\" font-size:10pt;\">von Forge etc wird lk-servers.de verwendet.</span></p><p><span style=\" font-size:10pt;\">Dies macht die Aktionen weniger Fehleranf\303\244llig, </span></p><p><span style=\" font-size:10pt;\">bietet aber nicht immer die neueste Version.</span></p><p><span style=\" font-size:10pt; font-weight:600;\">Developer Modus: </span><span style=\" font-size:10pt;\">Stellt f\303\274r Interessierte einige Debug Informationen</span></p><p><span style=\" font-size:10pt;\">in Logs bereit.</span></p></body></html>", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Settings: public Ui_Settings {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGS_H
