/********************************************************************************
** Form generated from reading UI file 'mainclass.ui'
**
** Created by: Qt User Interface Compiler version 5.12.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINCLASS_H
#define UI_MAINCLASS_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainClassClass
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout_3;
    QVBoxLayout *verticalLayout_6;
    QLabel *label;
    QFrame *line;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout;
    QCheckBox *terramenu_check;
    QCheckBox *optifine_check;
    QCheckBox *forge_check;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QCheckBox *sfanver_check;
    QCheckBox *sfaith_check;
    QCheckBox *chroma_check;
    QCheckBox *purebd_check;
    QSpacerItem *verticalSpacer_2;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer;
    QPushButton *settings_button;
    QPushButton *continue_button;
    QPushButton *quit_button;

    void setupUi(QMainWindow *MainClassClass)
    {
        if (MainClassClass->objectName().isEmpty())
            MainClassClass->setObjectName(QString::fromUtf8("MainClassClass"));
        MainClassClass->resize(337, 260);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainClassClass->sizePolicy().hasHeightForWidth());
        MainClassClass->setSizePolicy(sizePolicy);
        MainClassClass->setMaximumSize(QSize(337, 260));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/Resources/icons8-t-96.png"), QSize(), QIcon::Normal, QIcon::On);
        MainClassClass->setWindowIcon(icon);
        centralWidget = new QWidget(MainClassClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout_3 = new QHBoxLayout(centralWidget);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_6->addWidget(label);

        line = new QFrame(centralWidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout_6->addWidget(line);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayout = new QVBoxLayout(groupBox_2);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        terramenu_check = new QCheckBox(groupBox_2);
        terramenu_check->setObjectName(QString::fromUtf8("terramenu_check"));

        verticalLayout->addWidget(terramenu_check);

        optifine_check = new QCheckBox(groupBox_2);
        optifine_check->setObjectName(QString::fromUtf8("optifine_check"));

        verticalLayout->addWidget(optifine_check);

        forge_check = new QCheckBox(groupBox_2);
        forge_check->setObjectName(QString::fromUtf8("forge_check"));

        verticalLayout->addWidget(forge_check);


        verticalLayout_4->addWidget(groupBox_2);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_4->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout_4);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        sfanver_check = new QCheckBox(groupBox);
        sfanver_check->setObjectName(QString::fromUtf8("sfanver_check"));

        verticalLayout_2->addWidget(sfanver_check);

        sfaith_check = new QCheckBox(groupBox);
        sfaith_check->setObjectName(QString::fromUtf8("sfaith_check"));

        verticalLayout_2->addWidget(sfaith_check);

        chroma_check = new QCheckBox(groupBox);
        chroma_check->setObjectName(QString::fromUtf8("chroma_check"));

        verticalLayout_2->addWidget(chroma_check);

        purebd_check = new QCheckBox(groupBox);
        purebd_check->setObjectName(QString::fromUtf8("purebd_check"));

        verticalLayout_2->addWidget(purebd_check);


        verticalLayout_3->addWidget(groupBox);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);


        horizontalLayout->addLayout(verticalLayout_3);


        verticalLayout_5->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        settings_button = new QPushButton(centralWidget);
        settings_button->setObjectName(QString::fromUtf8("settings_button"));

        horizontalLayout_2->addWidget(settings_button);

        continue_button = new QPushButton(centralWidget);
        continue_button->setObjectName(QString::fromUtf8("continue_button"));
        continue_button->setFlat(false);

        horizontalLayout_2->addWidget(continue_button);

        quit_button = new QPushButton(centralWidget);
        quit_button->setObjectName(QString::fromUtf8("quit_button"));

        horizontalLayout_2->addWidget(quit_button);


        verticalLayout_5->addLayout(horizontalLayout_2);


        verticalLayout_6->addLayout(verticalLayout_5);


        horizontalLayout_3->addLayout(verticalLayout_6);

        MainClassClass->setCentralWidget(centralWidget);

        retranslateUi(MainClassClass);

        QMetaObject::connectSlotsByName(MainClassClass);
    } // setupUi

    void retranslateUi(QMainWindow *MainClassClass)
    {
        MainClassClass->setWindowTitle(QApplication::translate("MainClassClass", "MainClass", nullptr));
        label->setText(QApplication::translate("MainClassClass", "<html><head/><body><p><span style=\" font-size:14pt; font-weight:600;\">TerraOptimizer</span></p></body></html>", nullptr));
        groupBox_2->setTitle(QApplication::translate("MainClassClass", "Mods/Optimierung", nullptr));
        terramenu_check->setText(QApplication::translate("MainClassClass", "Terraconia Hauptmen\303\274", nullptr));
        optifine_check->setText(QApplication::translate("MainClassClass", "OptiFine", nullptr));
        forge_check->setText(QApplication::translate("MainClassClass", "Forge", nullptr));
        groupBox->setTitle(QApplication::translate("MainClassClass", "Ressourcenpakete", nullptr));
        sfanver_check->setText(QApplication::translate("MainClassClass", "Soartex Fanver", nullptr));
        sfaith_check->setText(QApplication::translate("MainClassClass", "Soartex Faithful", nullptr));
        chroma_check->setText(QApplication::translate("MainClassClass", "Chroma Hills", nullptr));
        purebd_check->setText(QApplication::translate("MainClassClass", "Sphax PureBDcraft", nullptr));
        settings_button->setText(QApplication::translate("MainClassClass", "Einstellungen", nullptr));
        continue_button->setText(QApplication::translate("MainClassClass", "Weiter", nullptr));
        quit_button->setText(QApplication::translate("MainClassClass", "Beenden", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainClassClass: public Ui_MainClassClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINCLASS_H
