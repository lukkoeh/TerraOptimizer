#ifndef SETTINGS_H
#define SETTINGS_H

#include <QDialog>
#include "mainclass.h"

namespace Ui {
class Settings;
}

class Settings : public QDialog
{
    Q_OBJECT

public:
    explicit Settings(QWidget *parent = nullptr);
	bool getLkm();
	bool getDev();
	bool lkm;
	bool dev;
    ~Settings();
public slots:
	void setMirrorVar();
	void setDevVar();

private:
    Ui::Settings *ui;
};

#endif // SETTINGS_H
