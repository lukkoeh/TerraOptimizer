#include "settings.h"
#include "ui_settings.h"
#include "mainclass.h"

Settings::Settings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Settings)
{
    ui->setupUi(this);
	connect(ui->lkmirror_check, SIGNAL(stateChanged(int)), this, SLOT(setMirrorVar()));
	connect(ui->dev_check, SIGNAL(stateChanged(int)), this, SLOT(setDevVar()));
}

bool Settings::getLkm() {
	return lkm;
}

bool Settings::getDev() {
	return dev;
}

Settings::~Settings()
{
    delete ui;
}

void Settings::setMirrorVar() {
	if (ui->lkmirror_check->isChecked()) {
		lkm = true;
	}
	if (ui->lkmirror_check->isChecked() == false) {
		lkm = false;
	}
}

void Settings::setDevVar() {
	if (ui->dev_check->isChecked()) {
		dev = true;
	}
	if (ui->dev_check->isChecked() == false) {
		dev = false;
	}
}